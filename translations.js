const config = require('./config.js')

module.exports = {
  english: {
    about: 'This bot was made by @MatiasConTilde with the idea of Pablo.\n\nIt is written in JavaScript/[Node.js](nodejs.org) with the [node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api) library.\nThe source code is published [on GitLab](https://gitlab.com/MatiasConTilde/TelegramVertretungBot).\nJoin the '+config.SUPPORT_CHANNEL+' channel to recieve news about updates and vote on new features!',
    accountDeleted: 'Account deleted',
    accountNotExisting: 'Account doesnt exist!\n/help?',
    accountUpdated: 'Account updated',
    class: 'Class',
    english: 'English',
    fach: 'Subject',
    help: 'Welcome to the Telegram Vertretungen bot!\nWith this bot, you can get notifications every morning about the substitutions you have.\n\nYou will first have to setup an account, so I know in which class you go and at what time you want to receive notifications. To start, send /settings, press on the buttons that appear and follow the instructions that you get later. You can also use this command to change the information later. You must configure at least Class and Time for it to work properly. If no language is specified, spanish is used.',
    info: 'More info',
    german: 'German',
    language: 'Language',
    lehrer: 'Teacher',
    nachrichten: 'News of the day',
    noSubstitutions: 'No Substitutions',
    nothing: 'Nothing (yet)',
    ok: 'Ok',
    privacy: 'This bot saves the following data about you, which is vital for working properly:\n*Your Telegram ID*: this identifies your Telegram account so it knows who to send your substitutions\n*Your class*: for sending the substitutions of the correct class.\n*Your preferred time*: to know at what time you would like to recieve your daily substitutions.\n\nYou can see all your stored information and delete it in /settings.\nNote that you need your parent\'s permision to use this bot if you are less than 16 years old.',
    raum: 'Room',
    requestNewClass: 'Send your class',
    requestNewLang: 'Send your preferred language',
    requestNewTime: 'Send your preferred time (in 24h format)',
    spanish: 'Spanish',
    stattFach: 'Instead of subject',
    stattLehrer: 'Instead of teacher',
    stunde: 'Hour',
    time: 'Preferred time',
    today: 'Today',
    tomorrow: 'Tomorrow',
    yourInfo: 'Your info',

    settingsKeyboard: [
      [{
        text: 'Class'
      }, {
        text: 'Time'
      }],
      [{
        text: 'Language'
      }, {
        text: 'Delete account'
      }],
      [{
        text: 'Cancel'
      }]
    ],
    languageKeyboard: [
      [{
        text: 'Spanish'
      }, {
        text: 'German'
      }, {
        text: 'English'
      }]
    ]
  },
  spanish: {
    about: 'Este bot fue hecho por @MatiasConTilde con la idea de Pablo.\n\nEstá escrito en JavaScript/[Node.js](nodejs.org) con la librería [node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api).\nEl código fuente está publicado [en GitLab](https://gitlab.com/MatiasConTilde/TelegramVertretungBot).\n¡Únete al canal '+config.SUPPORT_CHANNEL+' para recibir noticias sobre actualizaciones y votar nuevas funciones!',
    accountDeleted: 'Cuenta borrada',
    accountNotExisting: 'Cuenta no existe!\n/ayuda?',
    accountUpdated: 'Cuenta actualizada',
    class: 'Clase',
    english: 'Inglés',
    fach: 'Asignatura',
    help: '¡Bienvenido al bot de sustituciones de Telegram!\nCon este bot puedes recibir notificaciones cada mañana sobre las sustituciones que tienes.\n\nPrimero tendrás que crear una cuenta, para que sepa a qué clase vas y a qué hora quieres recibir las notificaciones. Para empezar, manda el comando /ajustes, dale a los botones que aparecen y sigue las instrucciones que recibes después. También puedes usar este comando para cambiar la información más tarde. Tienes que configurar por lo menos clase y tiempo para que funcione bien. Si no especificas el lenguaje, se usará español.',
    info: 'Más información',
    german: 'Alemán',
    language: 'Lenguaje',
    lehrer: 'Profesor',
    nachrichten: 'Noticias del día',
    noSubstitutions: 'Sin sustituciones',
    nothing: 'Nada (todavía)',
    ok: 'Ok',
    privacy: 'Este bot guarda la siguiente información sobre ti, que se necesita para su funcionamiento correcto:\n*Tu ID de Telegram*: esto identifica tu cuenta de Telegram para saber a quién mandar tus sustituciones.\n*Tu clase*: para recibir las sustituciones correctas.\n*Tu tiempo preferido*: para saber a qué tiempo quieres recibir tus sustituciones diarias.\n\nPuedes ver toda tu información guardada y borrarla en /ajustes.\nRecuerda que necesitas tener el permiso de tus padres para usar este bot si eres menor de 16 años.',
    raum: 'Aula',
    requestNewClass: 'Manda tu clase',
    requestNewLang: 'Manda tu lenguaje preferido',
    requestNewTime: 'Manda tu tiempo preferido (en formato 24h)',
    spanish: 'Español',
    stattFach: 'En lugar de asignatura',
    stattLehrer: 'En lugar de profesor',
    stunde: 'Hora',
    time: 'Tiempo preferido',
    today: 'Hoy',
    tomorrow: 'Mañana',
    yourInfo: 'Tu información',

    settingsKeyboard: [
      [{
        text: 'Clase'
      }, {
        text: 'Tiempo'
      }],
      [{
        text: 'Lenguaje'
      }, {
        text: 'Borrar cuenta'
      }],
      [{
        text: 'Cancelar'
      }]
    ],
    languageKeyboard: [
      [{
        text: 'Español'
      }, {
        text: 'Alemán'
      }, {
        text: 'Inglés'
      }]
    ]
  },
  german: {
    about: 'Dieser bot wurde von @MatiasConTilde mit der Idee von Pablo gemacht.\n\nEs ist in JavaScript/[Node.js](nodejs.org) mit der [node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api) Bibliothek geschrieben.\nDer Quellcode ist [auf GitLab](https://gitlab.com/MatiasConTilde/TelegramVertretungBot) veröffentlicht.\nTrete den Kanal '+config.SUPPORT_CHANNEL+' bei, um Nachrichten über Aktualisierungen zu erhalten und bei neuen Funktionen mitzubestimmen!',
    accountDeleted: 'Konto gelöscht',
    accountNotExisting: 'Konto existiert nicht!\n/hilfe?',
    accountUpdated: 'Konto aktualisiert',
    class: 'Klasse',
    english: 'Englisch',
    fach: 'Fach',
    help: 'Willkommen zum Telegram Vertretungen Bot!\nMit diesem Bot kannst du jeden Morgen Benachrichtigungen über die Vertretungen die du hast bekommen.\n\nZuerst musst du ein Konto erstellen, damit ich weiß, in welche Klasse du gehst und um welche Uhrzeit du die Benachrichtigungen bekommen willst. Um zu starten, schicke /einstellungen, drücke auf die Knöpfe, die erscheinen, und folge die Anweisungen, die du danach bekommst. Du kannst diesen Befehl auch später benutzen um manche informationen zu ändern. Du musst mindestens Klasse und Zeit konfigurieren, damit es funktioniert. Wenn keine Sprache spezifiziert ist, wird Spanisch benutzt.',
    info: 'Mehr info',
    german: 'Deutsch',
    language: 'Sprache',
    lehrer: 'Lehrer',
    nachrichten: 'Nachrichten zum Tag',
    noSubstitutions: 'Keine Vertretungen',
    nothing: '(Noch) nichts',
    ok: 'Ok',
    privacy: 'Dieser Bot speichert die folgenden Informationen über dich, die nötig für die korrekte Funktionsweise sind:\n*Deine Telegram ID*: dies identifiziert dein Telegram-Account um zu wissen, wem es deine Vertretungen schicken soll.\n*Deine Klasse*: um die korrekten Vertretungen zu schicken.\n*Deine bevorzugte Uhrzeit*: um zu wissen, wann du deine Vertretungen bekommen willst.\n\nDu kannst alle deine gespeicherten Informationen mit /einstellungen sehen und auch löschen.\nBeachte, dass du die Erlaubnis deiner Eltern brauchst, um diesen Bot zu benutzen, wenn du jünger als 16 Jahre alt bist.',
    raum: 'Raum',
    requestNewClass: 'Schicke deine Klasse',
    requestNewLang: 'Schicke deine bevorzugte Sprache',
    requestNewTime: 'Schicke deine bevorzugte Zeit (im 24h Format)',
    spanish: 'Spanisch',
    stattFach: 'Statt Fach',
    stattLehrer: 'Statt Lehrer',
    stunde: 'Stunde',
    time: 'Bevorzugte Zeit',
    today: 'Heute',
    tomorrow: 'Morgen',
    yourInfo: 'Deine info',

    settingsKeyboard: [
      [{
        text: 'Klasse'
      }, {
        text: 'Zeit'
      }],
      [{
        text: 'Sprache'
      }, {
        text: 'Konto löschen'
      }],
      [{
        text: 'Abbrechen'
      }]
    ],
    languageKeyboard: [
      [{
        text: 'Spanisch'
      }, {
        text: 'Deutsch'
      }, {
        text: 'Englisch'
      }]
    ]
  }
}
