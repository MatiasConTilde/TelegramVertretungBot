const config = require('./config.js');

const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot(config.API_KEY, {
  polling: true
});

const schedule = require('node-schedule');

const request = require('request');
const tabletojson = require('tabletojson');

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const db = low(new FileSync('db.json'));
db.defaults({
  users: []
}).write();
const users = db.get('users');

const translations = require('./translations.js');

let schedules = {};

bot.onText(/^(\/start|\/help|\/ayuda|\/hilfe)/, msg => send(msg.chat.id, 'help'));

bot.onText(/^\/about/, msg => send(msg.chat.id, 'about'));

bot.onText(/^(\/privacy|\/privacidad|\/datenschutz)/, msg => send(msg.chat.id, 'privacy'));

bot.onText(/^\/url/, msg => {
  const translation = getTranslation(msg.chat.id);
  const text = `[${translation.today}](${config.URL})\n[${translation.tomorrow}](${config.URL_TOMORROW})`;
  bot.sendMessage(msg.chat.id, text, {
    parse_mode: 'Markdown'
  });
});

bot.onText(/^(\/settings|\/ajustes|\/einstellungen)/, msg => {
  const account = users.find({
    id: msg.chat.id
  });

  const translation = getTranslation(msg.chat.id);
  let message = `*${translation.yourInfo}*\n`;
  if (account.value()) {
    message += `${translation.class}: ${(account.value().number + account.value().letter) || translation.nothing}\n`;
    message += `${translation.time}: ${account.value().time || translation.nothing}\n`;
    message += `${translation.language}: ${translation[account.value().language] || translation.nothing}`;
  } else {
    message += translation.nothing;
  }

  bot.sendMessage(msg.chat.id, message, {
    parse_mode: 'Markdown',
    reply_markup: {
      keyboard: translation.settingsKeyboard,
      resize_keyboard: true,
      selective: true
    }
  });
});

bot.onText(/^(Class|Clase|Klasse)$/, msg => send(msg.chat.id, 'requestNewClass', true));
bot.onText(/^([1-9]|1[0-2])([a-f])$/i, (msg, match) => {
  const account = users.find({
    id: msg.chat.id
  });

  if (account.value()) {
    account.assign({
      number: match[1],
      letter: match[2].toLowerCase()
    }).write();
  } else {
    users.push({
      id: msg.chat.id,
      number: match[1],
      letter: match[2].toLowerCase(),
      language: config.DEFAULT_LANGUAGE
    }).write();
  }

  send(msg.chat.id, 'accountUpdated');
});

bot.onText(/^(Time|Tiempo|Zeit)$/, msg => send(msg.chat.id, 'requestNewTime', true));
bot.onText(/^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$/, (msg, match) => {
  const account = users.find({
    id: msg.chat.id
  });

  if (account.value()) {
    account.assign({
      time: match.input
    }).write();
  } else {
    users.push({
      id: msg.chat.id,
      time: match.input,
      language: config.DEFAULT_LANGUAGE
    }).write();
  }

  startSchedule(msg.chat.id, match.input);

  send(msg.chat.id, 'accountUpdated');
});

bot.onText(/^(Language|Lenguaje|Sprache)$/, msg => {
  const translation = getTranslation(msg.chat.id);
  bot.sendMessage(msg.chat.id, translation.requestNewLang, {
    reply_markup: {
      keyboard: translation.languageKeyboard,
      resize_keyboard: true,
      selective: true
    }
  });
});
bot.onText(/^(English|Spanish|German|Inglés|Español|Alemán|Englisch|Spanisch|Deutsch)$/, (msg, match) => {
  const account = users.find({
    id: msg.chat.id
  });

  let language = config.DEFAULT_LANGUAGE;
  if (/(English|Inglés|Englisch)/.test(match.input)) language = 'english';
  else if (/(Spanish|Español|Spanisch)/.test(match.input)) language = 'spanish';
  else if (/(German|Alemán|Deutsch)/.test(match.input)) language = 'german';

  if (account.value()) {
    account.assign({
      language
    }).write();
  } else {
    users.push({
      id: msg.chat.id,
      language
    }).write();
  }

  send(msg.chat.id, 'accountUpdated', true);
});

bot.onText(/^(Delete account|Borrar cuenta|Konto löschen)$/, msg => {
  const account = users.find({
    id: msg.chat.id
  });
  if (account.value()) {
    send(msg.chat.id, 'accountDeleted', true);
    users.remove({
      id: msg.chat.id
    }).write();
  }
});

bot.onText(/^(Cancel|Cancelar|Abbrechen)$/, msg => send(msg.chat.id, 'ok', true));

bot.onText(/^\/heute/, msg => getSubstitutions(msg.chat.id, config.URL));

bot.onText(/^\/morgen/, msg => getSubstitutions(msg.chat.id, config.URL_TOMORROW));

const getSubstitutions = (id, url) => {
  const account = users.find({
    id
  }).value();
  if (account) {
    request.get(url, (err, res, body) => {
      const day = /<div class="mon_title">(.+)<\/div>/.exec(body)[1];
      // Use a promise to ensure it gets sent before the rest
      bot.sendMessage(id, `*${day}*`, {
        parse_mode: 'Markdown'
      }).then(() => {
        const translation = getTranslation(id);

        const tables = tabletojson.convert(body);

        if (tables.length > 1) {
          const news = tables[tables.length - 2].map(entry => entry['Nachrichten zum Tag']);
          if (news.every(val => val != undefined)) {
            const message = news.reduce((acc, val) => `${acc}${val}\n\n`, `*${translation.nachrichten}:*\n`);
            bot.sendMessage(id, message, {
              parse_mode: 'Markdown'
            });
          }
        }

        let noSubstitutions = true;
        for (entry of tables[tables.length - 1]) {
          if (entry['Klasse(n)'].includes(account.number) && entry['Klasse(n)'].includes(account.letter)) {
            noSubstitutions = false;

            let message = '';
            if (entry['Stunde']) message += `*${translation.stunde}:* _${entry['Stunde']}_\n`;
            if (entry['Lehrer']) message += `*${translation.lehrer}:* _${entry['Lehrer']}_\n`;
            if (entry['Fach']) message += `*${translation.fach}:* _${entry['Fach']}_\n`;
            if (entry['(Fach)']) message += `*${translation.stattFach}:* _${entry['(Fach)']}_\n`;
            if (entry['(Lehrer)']) message += `*${translation.stattLehrer}:* _${entry['(Lehrer)']}_\n`;
            if (entry['Raum'] != '---') message += `*${translation.raum}:* _${entry['Raum']}_\n`;
            if (entry['Vertretungs-Text']) message += `*${translation.info}:* _${entry['Vertretungs-Text']}_\n`;

            bot.sendMessage(id, message, {
              parse_mode: 'Markdown'
            });
          }
        }

        if (noSubstitutions) {
          send(id, 'noSubstitutions');
        }
      });
    });
  } else {
    send(id, 'accountNotExisting');
  }
};

const send = (id, msg, removeKeyboard) => bot.sendMessage(id, getTranslation(id)[msg], {
  parse_mode: 'Markdown',
  disable_web_page_preview: true,
  reply_markup: {
    remove_keyboard: removeKeyboard,
    selective: true
  }
});

const getTranslation = id => {
  const user = users.find({
    id
  }).value();

  if (user) {
    return translations[user.language];
  } else {
    return translations[config.DEFAULT_LANGUAGE];
  }
};

const startSchedule = (id, time) => {
  const colonIndex = time.indexOf(':');

  const rule = new schedule.RecurrenceRule();
  rule.hour = Number(time.substr(0, colonIndex));
  rule.minute = Number(time.substr(colonIndex + 1));
  rule.dayOfWeek = [new schedule.Range(1, 5)];

  if (schedules[id]) {
    schedules[id].reschedule(rule);
  } else {
    schedules[id] = schedule.scheduleJob(rule, getSubstitutions.bind(null, id, config.URL));
  }
};

// Start cronjobs for all users on startup
for (user of users.value()) {
  if (user.time) startSchedule(user.id, user.time);
}
