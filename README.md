# TelegramVertretungBot
Telegram bot that notifies users with the substitutions of the day

## Install
Get an Telegram API Token by talking to [@BotFather](https://t.me/BotFather)

Create `config.js` file and fill it with
```js
module.exports = {
  API_KEY: '123456789:ABCDEFGHIJKLMOPQRSTUVWXYZabcdefghij',
  URL: 'https://example.com/some-page-with-an-html-table',
  URL_TOMORROW: 'https://example.com/some-page-with-the-next-days-html-table',
  DEFAULT_LANGUAGE: 'english (or whatever)',
  SUPPORT_CHANNEL: '@ChannelWithSupport'
}
```
and edit the information to yours

Then:
```bash
npm install
```

## Run
```bash
npm start
```
